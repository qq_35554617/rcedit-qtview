# rcedit-qtview


## 简要
将github中的开源项目rcedit

[https://github.com/electron/rcedit](https://github.com/electron/rcedit "https://github.com/electron/rcedit")

[https://github.com/electron/rcedit?tab=readme-ov-file#rcedit](https://github.com/electron/rcedit?tab=readme-ov-file#rcedit "https://github.com/electron/rcedit?tab=readme-ov-file#rcedit")

进行封装成一个rceditPackage.dll，根据main中的命令函参数
定义成多个WINAPI函数

	RESCLEAPI_API const wchar_t* GetErrorStr();
	RESCLEAPI_API bool Get_Version_String(wchar_t* filepath, wchar_t* key, PTCHAR& Val);
	RESCLEAPI_API bool Get_Version_String_Batch(std::wstring filepath, std::map<std::wstring, std::wstring>& Mapval);
	RESCLEAPI_API bool Set_Version_String_Batch(std::wstring filepath, std::map<std::wstring, std::wstring> Mapval);
	
	RESCLEAPI_API bool Set_Version_String(wchar_t* _filepath, wchar_t* _key, wchar_t* _val);
	RESCLEAPI_API bool Set_File_Version(wchar_t* _filepath, wchar_t* _fileversion);
	RESCLEAPI_API bool Set_Product_Version(wchar_t* _filepath, wchar_t* _productversion);
	RESCLEAPI_API bool Set_Icon(wchar_t* _filepath, wchar_t* _pathicon);
	RESCLEAPI_API bool Set_Requested_Execution_Level(wchar_t* _filepath, wchar_t* _level);
	RESCLEAPI_API bool Application_Manifest(wchar_t* _filepath, wchar_t* _manifest);
	RESCLEAPI_API bool Set_Resource_String(wchar_t* _filepath, wchar_t* _key, wchar_t* _val);
	RESCLEAPI_API bool Set_Rcdata(wchar_t* _filepath, wchar_t* _key, wchar_t* _pathToResource);
	RESCLEAPI_API bool Get_Resource_String(wchar_t* _filepath, wchar_t* _key, PTCHAR& Val);

再使用Qt编写的一个可视化的界面
动态调用函数WINAPI修改exe/dll的版本信息工具
![](./preview.png)

## 开发环境
Qt 是使用的qt 5.13.1版本
**Mscv2017 x86 release** 编译

**rceditPackage.dll** 是
**Visual Studio 2017 x86位**编译生成的

## 注意
== 使用此工具修改软件或dll的版本号会造成数字签名或证书失效! ==

