﻿#include "lib_rescleapi.h"
#include <QApplication>

Lib_RescleApi::Lib_RescleApi()
{
    INITWINAPI();
}


void Lib_RescleApi::INITWINAPI()
{
    HMODULE H= NULL;
    QString dllpath=QApplication::applicationDirPath()+"/rceditPackage.dll";
    const wchar_t* wszLibraryName=utf8_to_wchar(dllpath.toStdString().c_str());
    qDebug()<<"DLL: "<<QString::fromWCharArray(wszLibraryName);
    if ((H = GetModuleHandleW(wszLibraryName)) != NULL)
        goto out;
    qDebug()<<"LoadLibraryExW -->";
    H = LoadLibraryExW(wszLibraryName, NULL, LOAD_LIBRARY_SEARCH_SYSTEM32);
    if(H==NULL)
        qDebug("Unable to load '%S.dll'", wszLibraryName);

    qDebug()<<"Load Function -->";
    pfGetErrorStr              = (GetErrorStr_W)              GetProcAddress(H,"GetErrorStr");
    pfGet_Version_String       = (Get_Version_String_W)       GetProcAddress(H,"Get_Version_String");
    pfGet_Version_String_Batch = (Get_Version_String_Batch_W) GetProcAddress(H,"Get_Version_String_Batch");
    pfSet_Version_String_Batch = (Set_Version_String_Batch_W) GetProcAddress(H,"Set_Version_String_Batch");

    if(pfGetErrorStr==NULL||pfGetErrorStr==nullptr)
        qDebug()<<"Not Load GetErrorStr Function!";
    if(pfGet_Version_String==NULL||pfGet_Version_String==nullptr)
        qDebug()<<"Not Load Get_Version_String Function!";
    if(pfGet_Version_String_Batch==NULL||pfGet_Version_String_Batch==nullptr)
        qDebug()<<"Not Load Get_Version_String_Batch Function!";
    if(pfSet_Version_String_Batch==NULL||pfSet_Version_String_Batch==nullptr)
        qDebug()<<"Not Load Set_Version_String_Batch Function!";
out:
//    free((LPWSTR)wszLibraryName);
    sfree(wszLibraryName);
    return;
}
