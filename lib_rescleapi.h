#ifndef LIB_RESCLEAPI_H
#define LIB_RESCLEAPI_H
#if defined(_MSC_VER) && (_MSC_VER >= 1600)
# pragma execution_character_set("utf-8")
#endif

#include "global.h"
#include <QObject>
#include <QWidget>

//! 公司名称
#define RU_VS_COMPANY_NAME      L"CompanyName"
//! 文件描述
#define RU_VS_FILE_DESCRIPTION  L"FileDescription"
//! 文件版本
#define RU_VS_FILE_VERSION      L"FileVersion"
//! 内部名称
#define RU_VS_INTERNAL_NAME     L"InternalName"
//! 版权信息
#define RU_VS_LEGAL_COPYRIGHT   L"LegalCopyright"
//! 商标信息
#define RU_VS_LEGAL_TRADEMARKS  L"LegalTrademarks"
//! 原始文件名
#define RU_VS_ORIGINAL_FILENAME L"OriginalFilename"
//! 私有构建信息
#define RU_VS_PRIVATE_BUILD     L"PrivateBuild"
//! 产品名称
#define RU_VS_PRODUCT_NAME      L"ProductName"
//! 产品版本
#define RU_VS_PRODUCT_VERSION   L"ProductVersion"
//! 特殊构建信息
#define RU_VS_SPECIAL_BUILD     L"SpecialBuild"
//! 注释
#define RU_VS_COMMENTS          L"Comments"



#define isnotnull(_VAL_) (_VAL_!=NULL && _VAL_!=nullptr)

typedef const wchar_t* (WINAPI *GetErrorStr_W)();
typedef bool (WINAPI *Get_Version_String_W)(wchar_t* filepath, wchar_t* key, PTCHAR& Val);
typedef bool (WINAPI *Get_Version_String_Batch_W)(std::wstring filepath, std::map<std::wstring, std::wstring>& Mapval);
typedef bool (WINAPI *Set_Version_String_Batch_W)(std::wstring filepath, std::map<std::wstring, std::wstring> Mapval);


//! 定义交互的函数接口
class Lib_RescleApi
{
public:
    Lib_RescleApi();

    GetErrorStr_W pfGetErrorStr=nullptr;
    Get_Version_String_W pfGet_Version_String=nullptr;
    Get_Version_String_Batch_W pfGet_Version_String_Batch=nullptr;
    Set_Version_String_Batch_W pfSet_Version_String_Batch=nullptr;


private:
    //! 初始化系统api
    void INITWINAPI();


};

#endif // LIB_RESCLEAPI_H






