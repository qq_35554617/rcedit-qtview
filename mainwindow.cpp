﻿#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>
#include <QVariant>
#include <QFileDialog>
#include <QMessageBox>
#include <QGraphicsDropShadowEffect>



MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    Api=new Lib_RescleApi();
    qDebug()<<"PID: "<<GetCurrentProcessId();

    //! 创建阴影
    QGraphicsDropShadowEffect *shadowEffect = new QGraphicsDropShadowEffect(ui->frame_windows);
    shadowEffect->setOffset(0);
    shadowEffect->setBlurRadius(10);
    shadowEffect->setColor(QColor::fromRgb(0,0,0,60));
    ui->frame_windows->setGraphicsEffect(shadowEffect);

    //! 绑定键值
    ui->lineEdit_CompanyName->setProperty("Key",QVariant::fromValue(QString::fromWCharArray(RU_VS_COMPANY_NAME)));
    ui->lineEdit_FileDescription->setProperty("Key",QVariant::fromValue(QString::fromWCharArray(RU_VS_FILE_DESCRIPTION)));
    ui->lineEdit_FileVersion->setProperty("Key",QVariant::fromValue(QString::fromWCharArray(RU_VS_FILE_VERSION)));
    ui->lineEdit_InternalName->setProperty("Key",QVariant::fromValue(QString::fromWCharArray(RU_VS_INTERNAL_NAME)));
    ui->lineEdit_LegalCopyright->setProperty("Key",QVariant::fromValue(QString::fromWCharArray(RU_VS_LEGAL_COPYRIGHT)));
    ui->lineEdit_LegalTrademarks->setProperty("Key",QVariant::fromValue(QString::fromWCharArray(RU_VS_LEGAL_TRADEMARKS)));
    ui->lineEdit_OriginalFilename->setProperty("Key",QVariant::fromValue(QString::fromWCharArray(RU_VS_ORIGINAL_FILENAME)));
    ui->lineEdit_PrivateBuild->setProperty("Key",QVariant::fromValue(QString::fromWCharArray(RU_VS_PRIVATE_BUILD)));
    ui->lineEdit_ProductName->setProperty("Key",QVariant::fromValue(QString::fromWCharArray(RU_VS_PRODUCT_NAME)));
    ui->lineEdit_ProductVersion->setProperty("Key",QVariant::fromValue(QString::fromWCharArray(RU_VS_PRODUCT_VERSION)));
    ui->lineEdit_SpecialBuild->setProperty("Key",QVariant::fromValue(QString::fromWCharArray(RU_VS_SPECIAL_BUILD)));
    ui->lineEdit_Comments->setProperty("Key",QVariant::fromValue(QString::fromWCharArray(RU_VS_COMMENTS)));

}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_toolButton_pressed()
{
    QString filepath = QFileDialog::getOpenFileName(this, "选择文件","*","*(*)");
    if(filepath!="")
    {
        ui->lineEdit_Filepath->setText(filepath);

        KayValMap.clear();
        KayValMap.insert(std::make_pair(RU_VS_COMPANY_NAME,L""));
        KayValMap.insert(std::make_pair(RU_VS_FILE_DESCRIPTION,L""));
        KayValMap.insert(std::make_pair(RU_VS_FILE_VERSION,L""));
        KayValMap.insert(std::make_pair(RU_VS_INTERNAL_NAME,L""));
        KayValMap.insert(std::make_pair(RU_VS_LEGAL_COPYRIGHT,L""));
        KayValMap.insert(std::make_pair(RU_VS_LEGAL_TRADEMARKS,L""));
        KayValMap.insert(std::make_pair(RU_VS_ORIGINAL_FILENAME,L""));
        KayValMap.insert(std::make_pair(RU_VS_PRIVATE_BUILD,L""));
        KayValMap.insert(std::make_pair(RU_VS_PRODUCT_NAME,L""));
        KayValMap.insert(std::make_pair(RU_VS_PRODUCT_VERSION,L""));
        KayValMap.insert(std::make_pair(RU_VS_SPECIAL_BUILD,L""));
        KayValMap.insert(std::make_pair(RU_VS_COMMENTS,L""));

        if(isnotnull(Api->pfGet_Version_String_Batch))
        {
            if(!Api->pfGet_Version_String_Batch(filepath.toStdWString(),KayValMap))
            {
                qDebug().noquote()<<QString("部分函数[Get_Version_String_Batch]执行失败! \r\n 原因:\r\n %1")
                                     .arg(QString::fromWCharArray(Api->pfGetErrorStr()));
                if(KayValMap.size()==0)
                {
                    QMessageBox::warning(this,"提示",QString("函数[Get_Version_String_Batch]执行失败! \r\n 原因:\r\n %1")
                                         .arg(QString::fromWCharArray(Api->pfGetErrorStr())));
                }
            }
        }
        else
            QMessageBox::warning(this,"提示","未能加载rceditPackage.dll文件中的[Get_Version_String_Batch]函数!");

        QList<QLineEdit*> LineEditALL= ui->frame->findChildren<QLineEdit*>();
        foreach (QLineEdit* lineEdit, LineEditALL) {
           if(isnotnull(lineEdit))
           {
               if(lineEdit->property("Key").isValid())
               {
                  if(KayValMap.find(lineEdit->property("Key").toString().toStdWString())!=KayValMap.end() && KayValMap.size()>0)
                  {
                      lineEdit->setText(QString::fromWCharArray(KayValMap[lineEdit->property("Key").toString().toStdWString()].c_str()));
                      lineEdit->setEnabled(true);
                  }
                  else
                  {
                      lineEdit->setText("");
                      lineEdit->setEnabled(false);
                  }
               }
           }
        }

        if(KayValMap.size()>0)
        {
            ui->pBut_UpdateVersion->setEnabled(true);
            ui->checkBox->setEnabled(true);
        }
        else
        {
            ui->pBut_UpdateVersion->setEnabled(false);
            ui->checkBox->setEnabled(false);
        }

        ui->checkBox->blockSignals(true);
        ui->checkBox->setChecked(false);
        ui->checkBox->blockSignals(false);
    }
}


void MainWindow::on_pBut_UpdateVersion_clicked()
{
    if(!QFileInfo::exists(ui->lineEdit_Filepath->text()))
    {
        QMessageBox::information(this,"提示","请选择有效文件!");
        return;
    }


    std::map<std::wstring,std::wstring> KayValMap;
    QList<QLineEdit*> LineEditALL= ui->frame->findChildren<QLineEdit*>();
    foreach (QLineEdit* lineEdit, LineEditALL) {
       if(isnotnull(lineEdit))
       {
           if(lineEdit->property("Key").isValid() && lineEdit->isEnabled())
           {
                KayValMap.insert(std::make_pair(lineEdit->property("Key").toString().toStdWString(),lineEdit->text().toStdWString()));
           }
       }
    }

    if(KayValMap.size()==0)
    {
        QMessageBox::information(this,"提示","无可修改项!");
        return;
    }

    if(isnotnull(Api->pfSet_Version_String_Batch))
    {
       if(!Api->pfSet_Version_String_Batch(ui->lineEdit_Filepath->text().toStdWString(),KayValMap))
      {
           QMessageBox::warning(this,"提示",QString("函数[Set_Version_String_Batch]执行失败! \r\n 原因:\r\n %1")
                                .arg(QString::fromWCharArray(Api->pfGetErrorStr())));
           return;
      }
    }
    else
    {
        QMessageBox::warning(this,"提示","未能加载rceditPackage.dll文件中的Set_Version_String_Batch函数");
        return;
    }

    QMessageBox::information(this,"提示","修改完毕!");
}

void MainWindow::on_checkBox_clicked(bool checked)
{
    if(checked)
    {
        QList<QLineEdit*> LineEditALL= ui->frame->findChildren<QLineEdit*>();
        foreach (QLineEdit* lineEdit, LineEditALL) {
           if(isnotnull(lineEdit))
           {
               if(lineEdit->property("Key").isValid() && !lineEdit->isEnabled())
               {
                   lineEdit->setEnabled(true);
               }
           }
        }
    }
    else
    {
        if(KayValMap.size()==0)
            return;

        QList<QLineEdit*> LineEditALL= ui->frame->findChildren<QLineEdit*>();
        foreach (QLineEdit* lineEdit, LineEditALL) {
           if(isnotnull(lineEdit))
           {
               if(KayValMap.find(lineEdit->property("Key").toString().toStdWString())!=KayValMap.end())
                   lineEdit->setEnabled(true);
               else
                   lineEdit->setEnabled(false);
           }
        }
    }
}
