#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#if defined(_MSC_VER) && (_MSC_VER >= 1600)
# pragma execution_character_set("utf-8")
#endif

#include <QMainWindow>
#include "global.h"
#include "lib_rescleapi.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_toolButton_pressed();

    void on_pBut_UpdateVersion_clicked();

    void on_checkBox_clicked(bool checked);

private:
    Ui::MainWindow *ui;

    Lib_RescleApi* Api=nullptr;

    //! 当前文件的有效键值
    std::map<std::wstring,std::wstring> KayValMap;
};
#endif // MAINWINDOW_H
