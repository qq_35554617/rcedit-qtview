#pragma once


#ifndef RESCLEAPI_H
#define RESCLEAPI_H

#ifndef _UNICODE
#define _UNICODE
#endif

#ifndef UNICODE
#define UNICODE
#endif


// 设置 utf-8 编码格式
#if defined(_MSC_VER) && (_MSC_VER >= 1600)
# pragma execution_character_set("utf-8")
#endif

#include "pch.h"
#include "global.h"
#include "VersionInfo.h"
#include "ResourceUpdater.h"
#include "ScopedResourceUpdater.h"


#ifdef RESCLEAPI_EXPORTS
#define RESCLEAPI_API __declspec(dllexport)
#else
#define RESCLEAPI_API __declspec(dllimport)
#endif


#ifdef __cplusplus
extern "C" {
#endif

	RESCLEAPI_API const wchar_t* GetErrorStr();
	RESCLEAPI_API bool Get_Version_String(wchar_t* filepath, wchar_t* key, PTCHAR& Val);
	RESCLEAPI_API bool Get_Version_String_Batch(std::wstring filepath, std::map<std::wstring, std::wstring>& Mapval);
	RESCLEAPI_API bool Set_Version_String_Batch(std::wstring filepath, std::map<std::wstring, std::wstring> Mapval);
	
	RESCLEAPI_API bool Set_Version_String(wchar_t* _filepath, wchar_t* _key, wchar_t* _val);
	RESCLEAPI_API bool Set_File_Version(wchar_t* _filepath, wchar_t* _fileversion);
	RESCLEAPI_API bool Set_Product_Version(wchar_t* _filepath, wchar_t* _productversion);
	RESCLEAPI_API bool Set_Icon(wchar_t* _filepath, wchar_t* _pathicon);
	RESCLEAPI_API bool Set_Requested_Execution_Level(wchar_t* _filepath, wchar_t* _level);
	RESCLEAPI_API bool Application_Manifest(wchar_t* _filepath, wchar_t* _manifest);
	RESCLEAPI_API bool Set_Resource_String(wchar_t* _filepath, wchar_t* _key, wchar_t* _val);
	RESCLEAPI_API bool Set_Rcdata(wchar_t* _filepath, wchar_t* _key, wchar_t* _pathToResource);
	RESCLEAPI_API bool Get_Resource_String(wchar_t* _filepath, wchar_t* _key, PTCHAR& Val);

#ifdef __cplusplus
}
#endif


// 获取最新的错误信息
const wchar_t* GetErrorStr()
{
	return ErrorStr;
}

// 获取版本信息 键值 对应值
bool Get_Version_String( wchar_t* filepath,   wchar_t* key,  PTCHAR& Val)
{
	ErrorStr = L"";
	ResourceUpdater updater;
	if (!updater.Load(filepath))
	{
		ErrorStr = (std::wstring(L"Unable to load file: ") + std::wstring(filepath)).c_str();
		return  false;
	}

	const wchar_t* result = updater.GetVersionString(key);
	if (!result)
	{
		ErrorStr =L"Unable to get version string";
		return false;
	}
	Val = (PTCHAR)result;
	return true;
}

// 批量获取版本信息 键值 对应值
bool Get_Version_String_Batch( std::wstring filepath, std::map<std::wstring, std::wstring>& Mapval)
{
	bool IsAllSuccess = true;
	/*! 不跨平台所以使用 std::wstring 类型传参【string类型不兼容】
	* C++ 封装DLL遇到的一些坑爹问题与解决方案 
	* https://blog.csdn.net/sm9sun/article/details/84986875?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522455395b7b576c5a2ec009b6c5e2a80ff%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=455395b7b576c5a2ec009b6c5e2a80ff&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~sobaiduend~default-1-84986875-null-null.142^v100^pc_search_result_base6&utm_term=%E8%8E%B7%E5%8F%96%E4%B8%8D%E5%88%B0%E5%87%BD%E6%95%B0%E6%8C%87%E9%92%88extern%20c&spm=1018.2226.3001.4187
	*/
	ErrorStr = L"Get_Version_String_Batch: \r\n";
	
	ResourceUpdater updater;
	if (!updater.Load(filepath.c_str()))
	{
		ErrorStr = (std::wstring(L"Unable to load file: ") + std::wstring(filepath)).c_str();
		Mapval.clear();
		return  false;
	}

	std::vector<std::wstring> nonentity;
	for (const auto& keyVal : Mapval) {
	
		const wchar_t* result = updater.GetVersionString(keyVal.first.c_str());
		if (!result)
		{
			ErrorStr = (std::wstring(ErrorStr) + std::wstring(L"Unable to get version string: ") + keyVal.first + std::wstring(L" \r\n ")).c_str();
			IsAllSuccess = false;
			nonentity.push_back(keyVal.first);
			continue;
		}
		Mapval[keyVal.first] = std::wstring(result);
	}

	//! 移除不存在的项
	for (const auto& key : nonentity)
		Mapval.erase(key);

	return IsAllSuccess;
}

//! 批量修改版本信息
bool Set_Version_String_Batch( std::wstring filepath,  std::map<std::wstring, std::wstring> Mapval)
{
	bool IsAllSuccess = true;
	ResourceUpdater updater;
	if (!updater.Load(filepath.c_str()))
	{
		ErrorStr = (std::wstring(L"Unable to load file: ") + std::wstring(filepath)).c_str();
		return  false;
	}

	for (const auto& keyVal : Mapval)
	{
		const wchar_t* key = keyVal.first.c_str();
		const wchar_t* value = keyVal.second.c_str();

		//! 文件版本和产品版本单独处理
		if (std::wstring(key) == std::wstring(RU_VS_FILE_VERSION))
		{
			unsigned short v1, v2, v3, v4;
			if (!parse_version_string(value, &v1, &v2, &v3, &v4))
			{
				ErrorStr = (std::wstring(L"Unable to parse version string for FileVersion:") + std::wstring(value)).c_str();
				continue;
			}

			if (!updater.SetFileVersion(v1, v2, v3, v4))
			{
				ErrorStr = (std::wstring(L"Unable to change file version:") + std::wstring(value)).c_str();
				continue;
			}
		}
		else if (std::wstring(key) == std::wstring(RU_VS_PRODUCT_VERSION))
		{
			unsigned short v1, v2, v3, v4;
			if (!parse_version_string(value, &v1, &v2, &v3, &v4))
			{
				ErrorStr = (std::wstring(L"Unable to parse version string for ProductVersion:") + std::wstring(value)).c_str();
				continue;
			}

			if (!updater.SetProductVersion(v1, v2, v3, v4))
			{
				ErrorStr = (std::wstring(L"Unable to change product version:") + std::wstring(value)).c_str();
				continue;
			}
		}

		if (!updater.SetVersionString(key, value))
		{
			ErrorStr = (std::wstring(ErrorStr) + std::wstring(L"Unable to change version string: ") + keyVal.first + std::wstring(L" \r\n ")).c_str();
			IsAllSuccess = false;
			continue;
		}
	}

	if (!updater.Commit())
	{
		IsAllSuccess = false;
		ErrorStr = L"Unable to commit changes!";
	}
	return IsAllSuccess;
}

////////////////////////    封装main调用的函数    /////////////////////////////
//! 修改版本相关字段信息 --set-version-string 
bool Set_Version_String( wchar_t* _filepath,  wchar_t* _key,  wchar_t* _val)
{
	ResourceUpdater updater;
	if (!updater.Load(_filepath))
	{
		ErrorStr = (std::wstring(L"Unable to load file: ") + std::wstring(_filepath)).c_str();
		return  false;
	}

	if (!updater.SetVersionString(_key, _val))
	{
		ErrorStr = L"Unable to change version string";
		return  false;
	}

	if (!updater.Commit())
	{ 
		ErrorStr = L"Unable to commit changes";
		return  false;
	}
	return true;
}

//! 修改文件版本 --set-file-version
bool Set_File_Version( wchar_t* _filepath,  wchar_t* _fileversion)
{
	ResourceUpdater updater;
	if (!updater.Load(_filepath))
	{
		ErrorStr = (std::wstring(L"Unable to load file: ") + std::wstring(_filepath)).c_str();
		return  false;
	}

	unsigned short v1, v2, v3, v4;
	if (!parse_version_string(_fileversion, &v1, &v2, &v3, &v4))
	{
		ErrorStr= (std::wstring(L"Unable to parse version string for FileVersion:") + std::wstring(_fileversion)).c_str();
		return  false;
	}

	if (!updater.SetFileVersion(v1, v2, v3, v4))
	{
		ErrorStr = (std::wstring(L"Unable to change file version:") + std::wstring(_fileversion)).c_str();
		return  false;
	}

	if (!updater.SetVersionString(L"FileVersion", _fileversion))
	{
		ErrorStr = L"Unable to change FileVersion string";
		return  false;
	}

	if (!updater.Commit())
	{
		ErrorStr = L"Unable to commit changes";
		return  false;
	}
	return true;
}
 
//! 修改产品版本 --set-product-version
bool Set_Product_Version( wchar_t* _filepath,  wchar_t* _productversion)
{
	ResourceUpdater updater;
	if (!updater.Load(_filepath))
	{
		ErrorStr = (std::wstring(L"Unable to load file: ") + std::wstring(_filepath)).c_str();
		return  false;
	}

	unsigned short v1, v2, v3, v4;
	if (!parse_version_string(_productversion, &v1, &v2, &v3, &v4))
	{
		ErrorStr = (std::wstring(L"Unable to parse version string for ProductVersion:") + std::wstring(_productversion)).c_str();
		return  false;
	}

	if (!updater.SetProductVersion(v1, v2, v3, v4))
	{
		ErrorStr = (std::wstring(L"Unable to change product version:") + std::wstring(_productversion)).c_str();
		return  false;
	}

	if (!updater.SetVersionString(L"ProductVersion", _productversion))
	{
		ErrorStr = L"Unable to change ProductVersion string";
		return  false;
	}

	if (!updater.Commit())
	{
		ErrorStr = L"Unable to commit changes";
		return  false;
	}
	return true;
}

bool Set_Icon( wchar_t* _filepath,  wchar_t* _pathicon)
{
	if (std::wstring(_pathicon).empty())
	{
		ErrorStr = L"requires path to the icon! ";
		return false;
	}

	ResourceUpdater updater;
	if (!updater.Load(_filepath))
	{
		ErrorStr = (std::wstring(L"Unable to load file: ") + std::wstring(_filepath)).c_str();
		return false;
	}

	if (!updater.SetIcon(_pathicon))
	{
		ErrorStr = (std::wstring(L"Unable to set icon:") + std::wstring(_pathicon)).c_str();
		return false;
	}

	if (!updater.Commit())
	{
		ErrorStr = L"Unable to commit changes";
		return false;
	}
	return true;
}

bool Set_Requested_Execution_Level( wchar_t* _filepath,  wchar_t* _level)
{
	if (std::wstring(_level).empty())
	{
		ErrorStr = L"set-requested-execution-level requires asInvoker, highestAvailable or requireAdministrator! ";
		return false;
	}

	ResourceUpdater updater;
	if (!updater.Load(_filepath))
	{
		ErrorStr = (std::wstring(L"Unable to load file: ") + std::wstring(_filepath)).c_str();
		return false;
	}

	if (updater.IsApplicationManifestSet())
	{
		ErrorStr = L"set - requested - execution - level is ignored if --application - manifest is set";
		return false;
	}

	if (!updater.SetExecutionLevel(_level))
	{
		ErrorStr = (std::wstring(L"Unable to set execution level:") + std::wstring(_level)).c_str();
		return false;
	}

	if (!updater.Commit())
	{
		ErrorStr = L"Unable to commit changes";
		return false;
	}
	return true;
}

bool Application_Manifest( wchar_t* _filepath,  wchar_t* _manifest)
{
	if (std::wstring(_manifest).empty())
	{
		ErrorStr = L"application-manifest requires local path! ";
		return false;
	}

	ResourceUpdater updater;
	if (!updater.Load(_filepath))
	{
		ErrorStr = (std::wstring(L"Unable to load file: ") + std::wstring(_filepath)).c_str();
		return false;
	}

	if (updater.IsExecutionLevelSet())
	{
		ErrorStr = L"set-requested-execution-level is ignored if --application-manifest is set";
		return false;
	}

	if (!updater.SetApplicationManifest(_manifest))
	{
		ErrorStr = (std::wstring(L"Unable to set application manifest:") + std::wstring(_manifest)).c_str();
		return false;
	}

	if (!updater.Commit())
	{
		ErrorStr = L"Unable to commit changes";
		return false;
	}
	return true;

}

bool Set_Resource_String( wchar_t* _filepath,  wchar_t* _key,  wchar_t* _val)
{
	if (std::wstring(_key).empty() || std::wstring(_val).empty())
	{
		ErrorStr = L"set-resource-string requires int 'Key' and string 'Value'";
		return false;
	}

	ResourceUpdater updater;
	if (!updater.Load(_filepath))
	{
		ErrorStr = (std::wstring(L"Unable to load file: ") + std::wstring(_filepath)).c_str();
		return  false;
	}

	unsigned int key_id = 0;
	if (swscanf_s(_key, L"%d", &key_id) != 1)
	{
		ErrorStr = L"Unable to parse id";
		return false;
	}

	if (!updater.ChangeString(key_id, _val))
	{
		ErrorStr = L"Unable to change string";
		return false;
	}

	if (!updater.Commit())
	{
		ErrorStr = L"Unable to commit changes";
		return  false;
	}
	return true;
}

bool Set_Rcdata( wchar_t* _filepath,  wchar_t* _key,  wchar_t* _pathToResource)
{
	if (std::wstring(_key).empty() || std::wstring(_pathToResource).empty())
	{
		ErrorStr = L"set-rcdata requires int 'Key' and path to resource 'Value'";
		return false;
	}

	ResourceUpdater updater;
	if (!updater.Load(_filepath))
	{
		ErrorStr = (std::wstring(L"Unable to load file: ") + std::wstring(_filepath)).c_str();
		return  false;
	}

	unsigned int key_id = 0;
	if (swscanf_s(_key, L"%d", &key_id) != 1)
	{
		ErrorStr = L"Unable to parse id";
		return false;
	}

	if (!updater.ChangeRcData(key_id, _pathToResource))
	{
		ErrorStr = L"Unable to change string";
		return false;
	}

	if (!updater.Commit())
	{
		ErrorStr = L"Unable to commit changes";
		return  false;
	}
	return true;
}

bool Get_Resource_String( wchar_t* _filepath,   wchar_t* _key,  PTCHAR& Val)
{
	if (std::wstring(_key).empty() )
	{
		ErrorStr = L"get-resource-string requires int 'Key'";
		return false;
	}

	ResourceUpdater updater;
	if (!updater.Load(_filepath))
	{
		ErrorStr = (std::wstring(L"Unable to load file: ") + std::wstring(_filepath)).c_str();
		return  false;
	}

	unsigned int key_id = 0;
	if (swscanf_s(_key, L"%d", &key_id) != 1)
	{
		ErrorStr = L"Unable to parse id";
		return false;
	}

	const wchar_t* result = updater.GetString(key_id);
	if (!result)
	{
		ErrorStr = L"Unable to get resource string";
		return false;
	}
	Val = (PTCHAR)result;
	return true;
}



#endif //RESCLEAPI_H
